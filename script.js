class Header14 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initHeader14()
    }

    initHeader14 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Header14")
    }

}

window.customElements.define('fir-header-14', Header14, { extends: 'div' })
