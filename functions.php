<?php

namespace Fir\Pinecones\Header14;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Header14',
            'label' => 'Pinecone: Header14',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "A nav component with selectable social icons"
                ],
                [
                    'label' => 'Select a Menu',
                    'name' => 'navTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => 'Choose a menu for the middle column:',
                    'name' => 'nav_menu',
                    'type' => 'select',
                    'instructions' => "Nested navs will be turned into dropdowns on hover"
                ],
                [
                    'label' => 'Social Icons',
                    'name' => 'socialTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => 'Instagram',
                    'name' => 'instagram',
                    'type' => 'url',
                    'instructions' => "Enter your full Instagram url"
                ],
                [
                    'label' => 'YouTube',
                    'name' => 'youtube',
                    'type' => 'url',
                    'instructions' => "Enter your full YouTube url"
                ],
                [
                    'label' => 'Facebook',
                    'name' => 'facebook',
                    'type' => 'url',
                    'instructions' => "Enter your full Facebook url"
                ],
                [
                    'label' => 'TikTok',
                    'name' => 'tiktok',
                    'type' => 'text',
                    'instructions' => "Enter your TikTok handle"
                ],
                [
                    'label' => 'Twitter',
                    'name' => 'twitter',
                    'type' => 'text',
                    'instructions' => "Enter your Twitter handle"
                ],
                [
                    'label' => 'Snapchat',
                    'name' => 'text',
                    'type' => 'url',
                    'instructions' => "Enter your Snapchat handle"
                ],     
                [
                    'label' => 'LinkedIn',
                    'name' => 'linkedin',
                    'type' => 'url',
                    'instructions' => "Enter your LinkedIn url"
                ],         
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}

function acf_load_nav_menu_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();    
    
    // get the textarea value from options page without any formatting
    $choices = wp_get_nav_menus(); 

    // loop through array and add to field 'choices'
    if( is_array($choices) ) {        
        foreach( $choices as $choice ) {            
            $field['choices'][ $choice->slug ] =  $choice->name;
            
        }        
    }
    

    // return the field
    return $field;
    
}

add_filter('acf/load_field/name=nav_menu', __NAMESPACE__ . '\\acf_load_nav_menu_field_choices');

